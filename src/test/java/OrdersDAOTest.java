import org.junit.Test;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;


public class OrdersDAOTest {
    @Test
    public void OrdersDAOTest() throws SQLException {
        BaseQuery test = new BaseQuery("root", "root");
        OrdersDAO order = new OrdersDAO();
        HashMap<String, Integer> NoOfOnHold = new HashMap<>();
        Statement stat = null;
        String query = "SELECT customers.customerNumber, COUNT(orders.status) FROM classicmodels.customers, classicmodels.orders WHERE (customers.customerNumber = orders.customerNumber) && (status = 'On Hold') GROUP BY customerNumber";
        stat = test.con.createStatement();
        ResultSet rs = stat.executeQuery(query);
        ResultSet orders = test.useTable("orders");
        ResultSet customers = test.useTable("customers");
        while (rs.next()) {
            NoOfOnHold.put(rs.getString(1), rs.getInt(2));
        }
        assertEquals (NoOfOnHold, order.NoOfOrdersOnHold());

    }

}
