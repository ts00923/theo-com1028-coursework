import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;

public class BaseQueryTest {
    Connection connection;
    BaseQuery testCon;

    @Before
    public void Connect() {
        this.testCon = new BaseQuery( "root", "root");
        this.connection = testCon.con;

    }

    @Test
    public void checkResultSetData() throws SQLException {
        ResultSet rs = testCon.useTable("Customers");
        if (!rs.next()) {
            throw new SQLException("No data found");
        }
    }

    @Test
    public void closeStatementCloses() throws SQLException {
        Statement statement = connection.createStatement();
        testCon.closeStatement();
        assertTrue(statement.isClosed());
    }


    @After
    public void after() {testCon.closeConnection();}

}
