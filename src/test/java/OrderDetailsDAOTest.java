import org.junit.Test;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;


public class OrderDetailsDAOTest {
    @Test
    public void OrderDetailsDAOTest() throws SQLException {
        BaseQuery test = new BaseQuery("root", "root");
        OrderDetailsDAO orderdetails = new OrderDetailsDAO();
        HashMap<String, Double> RevenueTotal = new HashMap<>();
        Statement stat = null;
        String query = "SELECT products.productName, SUM(quantityOrdered * priceEach) as Revenue FROM classicmodels.Products, classicmodels.orderDetails WHERE (products.productCode = orderDetails.productCode) GROUP BY productName ORDER BY productName";
        stat = test.con.createStatement();
        ResultSet rs = stat.executeQuery(query);
        ResultSet orderDetails = test.useTable("orderdetails");
        ResultSet products = test.useTable("products");
        while (rs.next()) {
            RevenueTotal.put(rs.getString(1), rs.getDouble(2));
        }
        assertEquals(RevenueTotal, orderdetails.RevenuePerProduct());

    }



}
