import org.junit.Test;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static org.junit.Assert.*;


public class PaymentsDAOTest {
    @Test
    public void paymentsTwiceAveragePayment() throws SQLException {
        BaseQuery test = new BaseQuery("root", "root");
        PaymentsDAO payment = new PaymentsDAO();
        ArrayList<Double> amount = new ArrayList<>();
        Statement stat = null;
        String query = "SELECT amount FROM payments WHERE amount > ((SELECT AVG(amount) FROM payments)*2)";
        stat = test.con.createStatement();
        ResultSet rs = stat.executeQuery(query);
        ResultSet payments = test.useTable("payments");
        while (rs.next()) {
            amount.add(rs.getDouble(1));
        }
        assertEquals(amount, payment.PaymentsTwiceAverage());
    }



}
