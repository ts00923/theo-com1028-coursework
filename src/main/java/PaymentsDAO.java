import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PaymentsDAO {

    public PaymentsDAO() {
        super();
        Statement statement = null;

    }

    public ArrayList<Double> PaymentsTwiceAverage() throws SQLException {
        double TotalAllPayments = 0;
        double TwiceAveragePayment = 0;
        BaseQuery query = new BaseQuery("root", "root");
        ResultSet allPayments = query.useTable("Payments");
        ArrayList<Double> paymentsAboveDouble = new ArrayList<>();
        ArrayList<Payments> paymentsObjects = new ArrayList<Payments>();
        while (allPayments.next()) {
            Payments payments = new Payments(allPayments.getInt(1), allPayments.getString(2), allPayments.getDate(3), allPayments.getDouble(4));
            paymentsObjects.add(payments);
        }
        for (Payments pay : paymentsObjects) {
            TotalAllPayments = TotalAllPayments + pay.getAmount();
        }
        TwiceAveragePayment = 2 * (TotalAllPayments / paymentsObjects.size());

        for (Payments pay : paymentsObjects) {
            if (TwiceAveragePayment < pay.getAmount()) {
                paymentsAboveDouble.add(pay.getAmount());
            }
        }

         return paymentsAboveDouble;

    }
}
