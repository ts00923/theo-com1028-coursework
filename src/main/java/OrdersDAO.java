import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class OrdersDAO {

    public OrdersDAO () {
        super();
        Statement statement = null;
    }

    public HashMap<Integer, Integer> NoOfOrdersOnHold() throws SQLException {
        BaseQuery query = new BaseQuery(  "root",  "root");
        ResultSet allCustomers = query.useTable( "Customers");
        ResultSet allOrders = query.useTable("Orders");
        ArrayList<Orders> ordersobject = new ArrayList<>();
        ArrayList<Customers> customersobject = new ArrayList<>();
        HashMap<Integer, Integer> onHold = new HashMap<>();
        while (allCustomers.next()) {
            Customers customer = new Customers(allCustomers.getInt(1), allCustomers.getString(2), allCustomers.getString(3), allCustomers.getString(4), allCustomers.getString(5), allCustomers.getString(6), allCustomers.getString(7), allCustomers.getString(8), allCustomers.getString(9), allCustomers.getString(10), allCustomers.getString(11), allCustomers.getInt(12), allCustomers.getDouble(13));
            customersobject.add(customer);
        }
        while (allOrders.next()) {
            Orders order = new Orders(allOrders.getInt(1), allOrders.getDate(2), allOrders.getDate(3), allOrders.getDate(4), allOrders.getString(5), allOrders.getString(6), allOrders.getInt(7));
            ordersobject.add(order);
        }

        for (Customers customer : customersobject) {
            int i = 1;

            for (Orders order : ordersobject) {
                if ((customer.getCustomerNumber() == order.getCustomerNumber()) && order.getStatus().equals("On Hold")) {
                    onHold.put(customer.getCustomerNumber(), i);
                    i = i + 1;
                }
            }


        }

        return onHold;

    }

}
