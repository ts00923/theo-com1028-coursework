public class Payments {

    private int customerNumber = 0;
    private String checkNumber = null;
    private java.sql.Date paymentDate = null;
    private double amount = 0;

    public Payments(int customerNumber, String checkNumber, java.sql.Date paymentDate, double amount) {
        this.customerNumber = customerNumber;
        this.checkNumber = checkNumber;
        this.paymentDate = paymentDate;
        this.amount = amount;
    }

    public double getAmount() { return amount; }


}

