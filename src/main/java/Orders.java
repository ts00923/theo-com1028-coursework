import java.util.*;

public class Orders {

    private int orderNumber = 0;
    private Date orderDate = null;
    private Date requiredDate = null;
    private Date shippedDate = null;
    private String status  = null;
    private String comments = null;
    private int customerNumber = 0;

    public Orders (int orderNumber, Date orderDate, Date requiredDate, Date shippedDate, String status, String comments, int customerNumber) {
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.requiredDate = requiredDate;
        this.shippedDate = shippedDate;
        this.status = status;
        this.comments = comments;
        this.customerNumber = customerNumber;

    }

    public int getCustomerNumber () {return customerNumber;}

    public String getStatus () {return status;}
}
