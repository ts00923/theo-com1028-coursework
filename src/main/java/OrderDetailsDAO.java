import com.mysql.cj.x.protobuf.MysqlxCrud;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


public class OrderDetailsDAO {
    double ProductTotal = 0.00;
    public OrderDetailsDAO() {
        super();
        Statement statement = null;
    }

    public HashMap<String, Double> RevenuePerProduct() throws SQLException {
        BaseQuery query = new BaseQuery("root", "root");
        ResultSet allOrderDetails = query.useTable("OrderDetails");
        ResultSet allProducts = query.useTable("Products");
        ArrayList<OrderDetails> orderdetailsobject = new ArrayList<>();
        ArrayList<Products> productsobject = new ArrayList<>();
        HashMap<String, Double> Revenue = new HashMap<>();
        while (allOrderDetails.next()) {
            OrderDetails orderDetails = new OrderDetails(allOrderDetails.getInt(1), allOrderDetails.getString(2), allOrderDetails.getInt(3), allOrderDetails.getDouble(4), allOrderDetails.getInt(5));
            orderdetailsobject.add(orderDetails);
        }
        while (allProducts.next()) {
            Products products = new Products(allProducts.getString(1), allProducts.getString(2), allProducts.getString(3), allProducts.getString(4), allProducts.getString(5), allProducts.getString(6), allProducts.getInt(7), allProducts.getDouble(8), allProducts.getDouble(9));
            productsobject.add(products);
        }

       for (Products product : productsobject) {

            for (OrderDetails orderDetail : orderdetailsobject) {
                if (product.getProductCode().equals(orderDetail.getProductCode())) {
                    ProductTotal = ProductTotal + (orderDetail.getQuantityOrdered() * orderDetail.getPriceEach());
                }
            }
            ProductTotal = (double) Math.round(ProductTotal * 100) / 100;
            Revenue.put(product.getProductName(), ProductTotal);
            ProductTotal = 0.00;
        }
        return Revenue;

    }

}
