public class Products {

    private String productCode = null;
    private String productName = null;
    private String productLine = null;
    private String productScale = null;
    private String productVendor = null;
    private String productDescription = null;
    private int quantityInStock = 0;
    private double buyPrice = 0;
    private double MSRP = 0;

    public Products(String productCode, String productName, String productLine, String productScale, String productVendor, String productDescription, int quantityInStock, double buyPrice, double MSRP) {
        this.productCode = productCode;
        this.productName = productName;
        this.productLine = productLine;
        this.productScale = productScale;
        this.productVendor = productVendor;
        this.productDescription = productDescription;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public String getProductCode() {return this.productCode;}
    public String getProductName() {return this.productName;}

}
