public class Customers {

  private int customerNumber;
  private String customerName;
  private String contactLastName;
  private String contactFirstName;
  private String phone;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String state;
  private String postalCode;
  private String country;
  private int salesRepEmployeeNumber;
  private double creditLimit;

  public Customers (int customerNumber, String customerName, String contactFirstName, String contactLastName, String phone, String addressLine1, String addressLine2, String city, String state, String postalCode, String country,int salesRepEmployeeNumber, double creditLimit) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.contactFirstName = contactFirstName;
    this.contactLastName = contactLastName;
    this.phone = phone;
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.city = city;
    this.state = state;
    this.postalCode = postalCode;
    this.country = country;
    this.salesRepEmployeeNumber = salesRepEmployeeNumber;
    this.creditLimit = creditLimit;
  }

  public int getCustomerNumber () {return customerNumber;}
}
